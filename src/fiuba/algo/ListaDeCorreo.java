package fiuba.algo;

import java.util.ArrayList;

public class ListaDeCorreo implements PuedeRecibirMails {

	private String nombre;
	private ArrayList<PuedeRecibirMails> integrantes;

	public ListaDeCorreo(String nombre) {
		this.nombre = nombre;
		this.integrantes = new ArrayList<PuedeRecibirMails>();
	}

	public String obtenerNombre() {
		return this.nombre;
	}

	public void addIntegrante(PuedeRecibirMails integranteNuevo) {
		this.integrantes.add(integranteNuevo);

	}

	public void agregarCorreo(Correo correoNuevo, String carpeta) throws CarpetaNoExistenteException {
		for (PuedeRecibirMails cuentaActual : this.integrantes) {
			cuentaActual.agregarCorreo(correoNuevo, carpeta);
		}
	}
}
