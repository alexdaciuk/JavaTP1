package fiuba.algo;

import java.util.ArrayList;

public class Cuenta implements PuedeRecibirMails {

	private String mail;
	private String contrasenia;
	private ArrayList<Carpeta> carpetas;

	public Cuenta(String mail, String contrasenia) throws MailInvalidoException, ContraseniaInvalidaException {
		this.validarMail(mail);
		this.mail = mail;

		this.validarContrasenia(contrasenia);
		this.contrasenia = contrasenia;

		this.carpetas = new ArrayList<Carpeta>();
	}


	public boolean validarLogin(String mail, String contrasenia)
			throws ContraseniaIncorrectaException, MailIncorrectoException {

		if (!this.contrasenia.equals(contrasenia)) {
			throw new ContraseniaIncorrectaException();
		} else if (!this.mail.equals(mail)) {
			throw new MailIncorrectoException();
		}

		return true;
	}

	public int mensajesEnCarpeta(String carpeta) throws CarpetaNoExistenteException {
		return this.obtenerCarpeta(carpeta).obtenerCantidadDeMensajes();
	}

	public void agregarCarpeta(String carpeta) {
		Carpeta carpetaNueva = new Carpeta(carpeta);
		this.carpetas.add(carpetaNueva);
	}

	public void agregarCorreo(Correo correoNuevo, String carpeta) throws CarpetaNoExistenteException {
		this.obtenerCarpeta(carpeta).agregarCorreo(correoNuevo);
	}

	private void validarContrasenia(String contrasenia) throws ContraseniaInvalidaException {
		if (contrasenia.isEmpty()) {
			throw new ContraseniaInvalidaException();
		}
	}

	private void validarMail(String mail) throws MailInvalidoException {
		if (mail.isEmpty() || !mail.contains("@") || !mail.contains(".com")) {
			throw new MailInvalidoException();
		}
	}

	public String obtenerNombre() {
		return this.mail;
	}

	public String obtenerAsuntoUltimoMensajeRecibidoEnCarpeta(String carpeta) throws CarpetaNoExistenteException {
		return this.obtenerCarpeta(carpeta).obtenerAsuntoUltimoMensajeRecibido();
	}

	private Carpeta obtenerCarpeta(String carpeta) throws CarpetaNoExistenteException {
		for (Carpeta carpetaActual : this.carpetas) {
			if (carpetaActual.obtenerNombre().equals(carpeta)) {
				return carpetaActual;
			}
		}
		throw new CarpetaNoExistenteException();
	}

	public int obtenerCantidadDeCorreos() {
		int cantidadDeCorreos = 0;
		for (Carpeta carpetaActual : this.carpetas) {
			if (! carpetaActual.obtenerNombre().equals("Enviados"))
			cantidadDeCorreos += carpetaActual.obtenerCantidadDeMensajes();
		}
		return cantidadDeCorreos;
	}

	public ArrayList<String> copiadosUltimoMensajeRecibidoEnCarpeta(String carpeta) throws CarpetaNoExistenteException {
		return this.obtenerCarpeta(carpeta).obtenerCopiadosUltimoMensajeRecibido();
	}


	public ArrayList<Correo> obtenerCorreosCarpeta(String carpeta) throws CarpetaNoExistenteException {
		return this.obtenerCarpeta(carpeta).obtenerCorreos();
	}

}