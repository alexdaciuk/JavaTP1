package fiuba.algo;


public interface PuedeRecibirMails {

	abstract public void agregarCorreo(Correo correo, String carpeta) throws CarpetaNoExistenteException;

	abstract public String obtenerNombre();

}
