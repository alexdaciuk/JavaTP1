package fiuba.algo;

import java.util.List;

public class FiltroPorRemitente extends Filtros {

	private String carpeta;
	private List<String> filtros;

	public FiltroPorRemitente(String carpeta, List<String> filtros) {
		this.carpeta = carpeta;
		this.filtros = filtros;
		
	}

	@Override
	public String evaluarCorreo(Correo correo) {
		for (String remitenteAFiltrar : this.filtros){
			if (correo.obtenerRemitente().obtenerNombre().equals(remitenteAFiltrar)){
				return this.carpeta;
			}
		}
		return "";
	}

}
