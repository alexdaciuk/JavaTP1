package fiuba.algo;

public class Alias implements PuedeRecibirMails {

	private Cuenta cuentaAsociada;
	private String nombre;
	
	public Alias(String nombre, Cuenta cuentaAsociada) {
		this.nombre = nombre;
		this.cuentaAsociada = cuentaAsociada;
	}

	public void agregarCorreo(Correo correo, String carpeta) throws CarpetaNoExistenteException {
			this.cuentaAsociada.agregarCorreo(correo, carpeta);
	}

	public String obtenerNombre() {
		return this.nombre;
	}

}
