package fiuba.algo;

import java.util.ArrayList;
import java.util.List;

import javafx.util.Callback;

public class AlgoMail {

	private BaseDeUsuarios usuarios;
	private Cartero cartero;

	public AlgoMail() {
		this.usuarios = new BaseDeUsuarios();
		this.cartero = new Cartero(this.usuarios);
	}

	public boolean nuevaCuenta(String mail, String contrasenia) {

		try {
			return (usuarios.nuevaCuenta(mail, contrasenia));
		} catch (MailInvalidoException | ContraseniaInvalidaException e) {
			return false;
		}

	}

	public boolean loginUsuario(String mail, String contrasenia) {

		try {
			usuarios.login(mail, contrasenia);
		} catch (ContraseniaIncorrectaException | MailIncorrectoException | CuentaInexistenteException e) {
			return false;
		}

		return true;
	}

	public int mensajesEnCarpeta(String carpeta) {
		try {
			return usuarios.mensajesEnCarpeta(carpeta);
		} catch (CarpetaNoExistenteException e) {
			return (-1);
		}
	}

	public boolean estaUsuarioLogueado() {
		return usuarios.estaUsuarioLogueado();
	}

	public void logout() {
		usuarios.logout();

	}

	public int cantidadDeCorreos() {
		if (usuarios.estaUsuarioLogueado()) {
			return usuarios.cantidadDeCorreosUsuarioLogueado();
		} else {
			throw new RuntimeException();
		}

	}

	public boolean enviarCorreo(String destinatario, String asunto, String mensaje) {
		try {
			cartero.enviarCorreo(destinatario, asunto, mensaje);
		} catch (CuentaInexistenteException | CarpetaNoExistenteException e) {
			return false;
		}
		return true;
	}

	public String asuntoUltimoMensajeRecibidoEnCarpeta(String carpeta) {
		try {
			return usuarios.obtenerUsuarioLogueado().obtenerAsuntoUltimoMensajeRecibidoEnCarpeta(carpeta);
		} catch (CarpetaNoExistenteException e) {
			return "";
		}

	}

	public boolean nuevaListaCorreos(String nombre, List<String> integrantes) {
		try {
			usuarios.nuevaListaDeCorreos(nombre, integrantes);
		} catch (CuentaInexistenteException e) {
			return false;
		}
		return true;
	}

	public boolean enviarCorreo(String destinatario, List<String> listaCopia, String asunto, String mensaje) {
		try {
			this.cartero.enviarCorreo(destinatario, listaCopia, asunto, mensaje);
		} catch (CuentaInexistenteException | CarpetaNoExistenteException e) {
			return false;
		}

		return true;

	}

	public boolean enviarCorreo(String destinatario, List<String> listaCopia, List<String> listaCopiaOculta, String asunto,
			String mensaje) {
		try {
			this.cartero.enviarCorreo(destinatario, listaCopia, listaCopiaOculta, asunto, mensaje);
		} catch (CuentaInexistenteException | CarpetaNoExistenteException e) {
			return false;
		}
		return true;
	}

	public ArrayList<String> copiadosUltimoMensajeRecibidoEnCarpeta(String carpeta) throws CarpetaNoExistenteException {
		return this.usuarios.copiadosUltimoMensajeRecibidoEnCarpeta(carpeta);
	}

	public boolean nuevoAlias(String nombre, String mail) {
		try {
			this.usuarios.nuevoAlias(nombre, mail);
		} catch (CuentaInexistenteException e) {
			return false;
		}
		return true;
	}

	public void nuevaCarpeta(String nombre) {
		this.usuarios.agregarCarpeta(nombre);
		
	}

	public void agregarFiltrosDeAsuntoEnCarpeta(String carpeta, List<String> filtros) {
		this.cartero.agregarFiltrosDeAsuntoEnCarpeta(carpeta, filtros);
		
	}

	public void agregarFiltrosRemitenteEnCarpeta(String carpeta, List<String> filtros) {
		this.cartero.agregarFiltrosRemitenteEnCarpeta(carpeta, filtros);
	}

	public ArrayList<String> obtenerCuentas() {
		return this.usuarios.obtenerCuentas();
	}

	public ArrayList<Correo> obtenerCorreosCarpetaDeUsuario(String usuario, String carpeta) {
		try {
			return this.usuarios.obtenerCorreosCarpetaDeUsuario(usuario, carpeta);
		} catch (CarpetaNoExistenteException | CuentaInexistenteException e) {
			return new ArrayList<Correo>();
		}
	}

}
