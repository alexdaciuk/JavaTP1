package fiuba.algo;

import java.util.List;

public class FiltroDeAsunto extends Filtros {

	private String carpeta;
	private List<String> filtros;

	public FiltroDeAsunto(String carpeta, List<String> filtros) {
		this.carpeta = carpeta;
		this.filtros = filtros;
	}

	@Override
	public String evaluarCorreo(Correo correo) {
		for (String asuntoAFiltrar : this.filtros){
			if (correo.obtenerAsunto().contains(asuntoAFiltrar)){
				return this.carpeta;
			}
		}
		return "";
	}

}
