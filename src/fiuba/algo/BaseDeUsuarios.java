package fiuba.algo;

import java.util.ArrayList;
import java.util.List;

public class BaseDeUsuarios {

	private ArrayList<Cuenta> cuentas;
	private ArrayList<ListaDeCorreo> listas;
	private ArrayList<Alias> alias;
	private Cuenta usuarioLogueado;

	public BaseDeUsuarios() {
		this.cuentas = new ArrayList<Cuenta>();
		this.alias = new ArrayList<Alias>();
		this.listas = new ArrayList<ListaDeCorreo>();
		this.usuarioLogueado = null;
	}

	public boolean nuevaCuenta(String mail, String contrasenia)
			throws MailInvalidoException, ContraseniaInvalidaException {

		if (!this.existeMail(mail)) {
			Cuenta cuentaNueva = new Cuenta(mail, contrasenia);

			cuentaNueva.agregarCarpeta("Principal");
			cuentaNueva.agregarCarpeta("Enviados");

			this.cuentas.add(cuentaNueva);

			return true;
		} else {
			return false;

		}

	}

	public void nuevaListaDeCorreos(String nombre, List<String> integrantes) throws CuentaInexistenteException {
		ListaDeCorreo listaNueva = new ListaDeCorreo(nombre);

		for (String integranteActual : integrantes) {
			listaNueva.addIntegrante(this.obtenerCuenta(integranteActual));
		}

		this.listas.add(listaNueva);

	}

	public void nuevoAlias(String nombre, String mail) throws CuentaInexistenteException {
		Alias nuevoAlias = new Alias(nombre, this.obtenerCuenta(mail));

		this.alias.add(nuevoAlias);

	}

	public boolean login(String mail, String contrasenia)
			throws ContraseniaIncorrectaException, MailIncorrectoException, CuentaInexistenteException {

		if (this.obtenerCuenta(mail).validarLogin(mail, contrasenia)) {
			this.usuarioLogueado = this.obtenerCuenta(mail);
			return true;
		} else {
			return false;
		}

	}

	private Cuenta obtenerCuenta(String mail) throws CuentaInexistenteException {
		for (Cuenta miembroActual : this.cuentas) {
			if (miembroActual.obtenerNombre() == mail) {
				return miembroActual;
			}
		}
		throw new CuentaInexistenteException();
	}

	public PuedeRecibirMails obtenerMiembro(String mail) throws CuentaInexistenteException {
		for (PuedeRecibirMails miembroActual : this.cuentas) {
			if (miembroActual.obtenerNombre().equals(mail)) {
				return miembroActual;
			}
		}
		for (PuedeRecibirMails miembroActual : this.listas) {
			if (miembroActual.obtenerNombre().equals(mail)) {
				return miembroActual;
			}
		}
		for (PuedeRecibirMails miembroActual : this.alias) {
			if (miembroActual.obtenerNombre().equals(mail)) {
				return miembroActual;
			}
		}

		throw new CuentaInexistenteException();
	}

	private boolean existeMail(String mail) {
		for (PuedeRecibirMails miembroActual : this.cuentas) {
			if (miembroActual.obtenerNombre() == mail) {
				return true;
			}
		}
		return false;
	}

	public int mensajesEnCarpeta(String carpeta) throws CarpetaNoExistenteException {
		return this.usuarioLogueado.mensajesEnCarpeta(carpeta);

	}

	public Cuenta obtenerUsuarioLogueado() {
		return this.usuarioLogueado;
	}
	
	public boolean estaUsuarioLogueado() {
		return (this.usuarioLogueado != null);
	}

	public void logout() {
		this.usuarioLogueado = null;

	}

	public int cantidadDeCorreosUsuarioLogueado() {
		return this.usuarioLogueado.obtenerCantidadDeCorreos();
	}

	public ArrayList<String> copiadosUltimoMensajeRecibidoEnCarpeta(String carpeta) throws CarpetaNoExistenteException {
		return this.usuarioLogueado.copiadosUltimoMensajeRecibidoEnCarpeta(carpeta);
	}

	public void agregarCarpeta(String nombreCarpeta) {
		for (Cuenta miembroActual : this.cuentas) {
			miembroActual.agregarCarpeta(nombreCarpeta);
		}

	}

	public ArrayList<String> obtenerCuentas() {
		ArrayList<String> arrayCuentas = new ArrayList<String>();
		
		for (Cuenta cuentaActual : this.cuentas){
			arrayCuentas.add(cuentaActual.obtenerNombre());
		}
		
		return arrayCuentas;
		
		
	}

	public ArrayList<Correo> obtenerCorreosCarpetaDeUsuario(String usuario, String carpeta) throws CarpetaNoExistenteException, CuentaInexistenteException {
		return this.obtenerCuenta(usuario).obtenerCorreosCarpeta(carpeta);
	}

}
