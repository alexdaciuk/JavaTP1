package fiuba.algo;

import java.util.ArrayList;


public class Carpeta {

	private String nombre;
	private ArrayList<Correo> correos;

	public Carpeta(String nombre) {
		this.nombre = nombre;
		this.correos = new ArrayList<Correo>();
	}

	public String obtenerNombre() {
		return this.nombre;
	}

	public int obtenerCantidadDeMensajes() {
		return this.correos.size();
	}

	public void agregarCorreo(Correo correoNuevo) {
		this.correos.add(correoNuevo);

	}

	public String obtenerAsuntoUltimoMensajeRecibido() {
		return this.correos.get(this.correos.size()-1).obtenerAsunto();
	}

	public ArrayList<String> obtenerCopiadosUltimoMensajeRecibido() {
		return this.correos.get( this.correos.size() -1 ).obtenerCopiados();
	}

	public ArrayList<Correo> obtenerCorreos() {
		return this.correos;
	}
}
