package fiuba.algo;

import java.util.ArrayList;
import java.util.List;

public class Correo {

	private PuedeRecibirMails destinatario;
	private String asunto;
	private String mensaje;
	private PuedeRecibirMails remitente;
	private List<String> listaCopia;


	public Correo(PuedeRecibirMails remitente, List<String> listaCopia,
			PuedeRecibirMails destinatario, String asunto, String mensaje) {
		this.listaCopia = listaCopia;
		this.remitente = remitente;
		this.destinatario = destinatario;
		this.asunto = asunto;
		this.mensaje = mensaje;
	}

	public PuedeRecibirMails obtenerRemitente() {
		return this.remitente;
	}

	public PuedeRecibirMails obtenerDestinatario() {
		return this.destinatario;
	}

	public String obtenerAsunto() {
		return this.asunto;
	}

	public ArrayList<String> obtenerCopiados() {
		ArrayList<String> copiadosMasDestinatario = new ArrayList<String>();
		
		copiadosMasDestinatario.addAll(listaCopia);
		copiadosMasDestinatario.add(this.destinatario.obtenerNombre());
		
		return (copiadosMasDestinatario);
	}

	public String obtenerMensaje() {
		return this.mensaje;
	}

}
