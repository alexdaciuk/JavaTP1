package fiuba.algo;

import java.util.ArrayList;
import java.util.List;

public class Cartero {

	private BaseDeUsuarios usuarios;
	private ArrayList<Filtros> filtros;

	public Cartero(BaseDeUsuarios usuarios) {
		this.usuarios = usuarios;
		this.filtros = new ArrayList<Filtros>();
	}

	public void enviarCorreo(String destinatario, String asunto, String mensaje)
			throws CuentaInexistenteException, CarpetaNoExistenteException {
		PuedeRecibirMails referenciaDestinatario = this.usuarios.obtenerMiembro(destinatario);
		PuedeRecibirMails referenciaRemitente = this.usuarios.obtenerUsuarioLogueado();

		Correo correoNuevo = this.crearCorreo(null, asunto, mensaje, referenciaDestinatario, referenciaRemitente);

		String bandeja = this.evaluarFiltros(correoNuevo);

		this.agregarCorreosEnBandejas(referenciaDestinatario, referenciaRemitente, correoNuevo, bandeja);
	}

	private void agregarCorreosEnBandejas(PuedeRecibirMails referenciaDestinatario,
			PuedeRecibirMails referenciaRemitente, Correo correoNuevo, String bandeja)
					throws CarpetaNoExistenteException {
		referenciaDestinatario.agregarCorreo(correoNuevo, bandeja);
		referenciaRemitente.agregarCorreo(correoNuevo, "Enviados");
	}

	public void enviarCorreo(String destinatario, List<String> listaCopia, String asunto, String mensaje)
			throws CuentaInexistenteException, CarpetaNoExistenteException {
		PuedeRecibirMails referenciaDestinatario;

		referenciaDestinatario = this.usuarios.obtenerMiembro(destinatario);

		PuedeRecibirMails referenciaRemitente = this.usuarios.obtenerUsuarioLogueado();

		Correo correoNuevo = this.crearCorreo(listaCopia, asunto, mensaje, referenciaDestinatario, referenciaRemitente);
		String bandeja = this.evaluarFiltros(correoNuevo);
		enviarCorreoLista(listaCopia, referenciaDestinatario, referenciaRemitente, correoNuevo, bandeja);

	}

	public void enviarCorreo(String destinatario, List<String> listaCopia, List<String> listaCopiaOculta, String asunto,
			String mensaje) throws CuentaInexistenteException, CarpetaNoExistenteException {
		PuedeRecibirMails referenciaDestinatario;
		referenciaDestinatario = this.usuarios.obtenerMiembro(destinatario);
		PuedeRecibirMails referenciaRemitente = this.usuarios.obtenerUsuarioLogueado();

		Correo correoNuevo = crearCorreo(listaCopia, asunto, mensaje, referenciaDestinatario, referenciaRemitente);

		ArrayList<String> listasUnidas = new ArrayList<String>();
		listasUnidas.addAll(listaCopiaOculta);
		listasUnidas.addAll(listaCopia);

		String bandeja = this.evaluarFiltros(correoNuevo);

		enviarCorreoLista(listasUnidas, referenciaDestinatario, referenciaRemitente, correoNuevo, bandeja);

	}

	private Correo crearCorreo(List<String> listaCopia, String asunto, String mensaje,
			PuedeRecibirMails referenciaDestinatario, PuedeRecibirMails referenciaRemitente) {
		Correo correoNuevo = new Correo(referenciaRemitente, listaCopia, referenciaDestinatario, asunto, mensaje);
		return correoNuevo;
	}

	public void agregarFiltrosDeAsuntoEnCarpeta(String carpeta, List<String> filtros) {
		FiltroDeAsunto filtroNuevo = new FiltroDeAsunto(carpeta, filtros);

		this.filtros.add(filtroNuevo);

	}

	public void agregarFiltrosRemitenteEnCarpeta(String carpeta, List<String> filtros) {
		FiltroPorRemitente filtroNuevo = new FiltroPorRemitente(carpeta, filtros);

		this.filtros.add(filtroNuevo);

	}

	private void enviarCorreoLista(List<String> listaCopia, PuedeRecibirMails referenciaDestinatario,
			PuedeRecibirMails referenciaRemitente, Correo correoNuevo, String bandeja)
					throws CuentaInexistenteException, CarpetaNoExistenteException {
		for (String miembroActual : listaCopia) {
			PuedeRecibirMails miembroActualDebug = this.usuarios.obtenerMiembro(miembroActual);

			miembroActualDebug.agregarCorreo(correoNuevo, bandeja);

		}

		this.agregarCorreosEnBandejas(referenciaDestinatario, referenciaRemitente, correoNuevo, bandeja);
	}

	private String evaluarFiltros(Correo correoNuevo) {
		for (Filtros filtroActual : this.filtros) {
			String carpeta = filtroActual.evaluarCorreo(correoNuevo);
			if (!carpeta.isEmpty()) {
				return carpeta;
			}

		}
		return "Principal";
	}

}
