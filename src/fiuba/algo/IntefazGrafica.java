package fiuba.algo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class IntefazGrafica extends Application {

	private AlgoMail clienteCorreo;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		this.cargarDatos();

		stage.setTitle("Consulta correos enviados");

		Button botonConsultar = new Button();
		botonConsultar.setText("Consultar");

		ObservableList<String> options = FXCollections.observableArrayList(clienteCorreo.obtenerCuentas());
		final ComboBox menuCuentas = new ComboBox(options);

		final Label etiqueta = new Label();

		botonConsultar.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				etiqueta.setText(this.obtenerMensajesFormateados(menuCuentas.getValue().toString()));

			}

			private String obtenerMensajesFormateados(String usuario) {
				ArrayList<Correo> listaCorreos = clienteCorreo.obtenerCorreosCarpetaDeUsuario(usuario, "Enviados");

				StringBuilder correosFormateados = new StringBuilder();
				correosFormateados.append("Remitente: " + usuario + "\n");

				for (Correo correoActual : listaCorreos) {
					correosFormateados.append("\n");
					correosFormateados.append("Destinatario: " + correoActual.obtenerDestinatario().obtenerNombre() + "\n");
					correosFormateados.append("Asunto: " + correoActual.obtenerAsunto() + "\n");
					correosFormateados.append("Mensaje: " + correoActual.obtenerMensaje() + "\n");

				}
				return correosFormateados.toString();
			}
		});

		
		HBox contenedorVertical = new HBox(menuCuentas, botonConsultar);
		VBox contenedorPrincipal = new VBox(contenedorVertical, etiqueta);
		
		contenedorVertical.setSpacing(10);
		contenedorPrincipal.setSpacing(20);
		contenedorPrincipal.setPadding(new Insets(20));

		Scene scene = new Scene(contenedorPrincipal, 500, 450);

		stage.setScene(scene);
		stage.show();
	}

	private void cargarDatos() {
		clienteCorreo = new AlgoMail();
		clienteCorreo.nuevaCuenta("carlosperez@gmail.com", "carlitos");
		clienteCorreo.nuevaCuenta("juanperez@gmail.com", "juancito");
		clienteCorreo.nuevaCuenta("anaperez@gmail.com", "anita");
		clienteCorreo.nuevaCuenta("jorgeperez@gmail.com", "jorgito");
		clienteCorreo.nuevaCuenta("raulperez@gmail.com", "raulito");

		clienteCorreo.loginUsuario("carlosperez@gmail.com", "carlitos");
		clienteCorreo.enviarCorreo("juanperez@gmail.com", "Saludo", "Hola Juan!!");
		clienteCorreo.logout();

		List<String> primeraLista = Arrays.asList("juanperez@gmail.com", "anaperez@gmail.com");
		clienteCorreo.nuevaListaCorreos("juanYana", primeraLista);
		clienteCorreo.loginUsuario("juanperez@gmail.com", "juancito");
		clienteCorreo.enviarCorreo("juanYana", "Saludo", "Hola a todos!!");

		List<String> segundaLista = Arrays.asList("juanperez@gmail.com", "anaperez@gmail.com");
		clienteCorreo.nuevaListaCorreos("juanYana", segundaLista);
		clienteCorreo.loginUsuario("carlosperez@gmail.com", "carlitos");
		List<String> terceraLista = Arrays.asList("jorgeperez@gmail.com", "juanYana");
		clienteCorreo.enviarCorreo("raulperez@gmail.com", terceraLista, "Saludos", "Otros saludos!");

		clienteCorreo.loginUsuario("carlosperez@gmail.com", "carlitos");
		List<String> cuartaLista = Arrays.asList("anaperez@gmail.com");
		List<String> listaCopiaOculta = Arrays.asList("raulperez@gmail.com", "jorgeperez@gmail.com");
		clienteCorreo.enviarCorreo("juanperez@gmail.com", cuartaLista, listaCopiaOculta, "Saludos", "Hola!");

		clienteCorreo.nuevoAlias("charlie", "carlosperez@gmail.com");
		clienteCorreo.nuevoAlias("anita", "anaperez@gmail.com");
		clienteCorreo.loginUsuario("carlosperez@gmail.com", "carlitos");
		List<String> quintaLista = Arrays.asList("anita");
		clienteCorreo.enviarCorreo("charlie", quintaLista, "Saludos", "Saludos con alias!");

	}
}
