package fiuba.algo3.Tp1Test;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fiuba.algo.BaseDeUsuarios;
import fiuba.algo.ContraseniaIncorrectaException;
import fiuba.algo.ContraseniaInvalidaException;
import fiuba.algo.CuentaInexistenteException;
import fiuba.algo.MailIncorrectoException;
import fiuba.algo.MailInvalidoException;

public class BaseDeUsuariosTest {

	private static final String MAIL_VALIDO = "tp1Java@gmail.com";
	private static final String CONTRASENIA_VALIDA = "contrasenia";
	private BaseDeUsuarios usuarios;

	@Before
	public void setup() throws MailInvalidoException, ContraseniaInvalidaException {
		usuarios = new BaseDeUsuarios();
		usuarios.nuevaCuenta(MAIL_VALIDO, CONTRASENIA_VALIDA);
	}

	@Test
	public void puedoCrearUnaBaseDeUsuarios() {
		BaseDeUsuarios usuarios = new BaseDeUsuarios();
	}

	@Test(expected = fiuba.algo.MailInvalidoException.class)
	public void tiraExcepcionMailInvalidoExceptionSiMailVacio()
			throws MailInvalidoException, ContraseniaInvalidaException {
		usuarios.nuevaCuenta("", CONTRASENIA_VALIDA);
	}

	@Test(expected = fiuba.algo.ContraseniaInvalidaException.class)
	public void tiraExcepcionContraseniaInvalidaExceptionSiContraseñaVacia()
			throws MailInvalidoException, ContraseniaInvalidaException {
		usuarios.nuevaCuenta("usuario@gmail.com", "");
	}

	@Test(expected = fiuba.algo.MailInvalidoException.class)
	public void tiraExcepcionMailInvalidoExceptionSiMailNoTieneArroba()
			throws MailInvalidoException, ContraseniaInvalidaException {
		usuarios.nuevaCuenta("mailSinArroba", CONTRASENIA_VALIDA);
	}

	@Test(expected = fiuba.algo.MailInvalidoException.class)
	public void tiraExcepcionMailInvalidoExceptionSiMailNoTienePuntoCom()
			throws MailInvalidoException, ContraseniaInvalidaException {
		usuarios.nuevaCuenta("mailSin@PuntoCom", CONTRASENIA_VALIDA);
	}

	@Test
	public void puedoLoguearUsuarioValido() throws ContraseniaIncorrectaException, MailIncorrectoException, CuentaInexistenteException {
		Assert.assertTrue(usuarios.login(MAIL_VALIDO, CONTRASENIA_VALIDA));
	}

	@Test(expected = fiuba.algo.ContraseniaIncorrectaException.class)
	public void tiraExcepcionContraseniaIncorrectaExceptionSiContraseniaIncorrecta()
			throws ContraseniaIncorrectaException, MailIncorrectoException, CuentaInexistenteException {
		Assert.assertTrue(usuarios.login(MAIL_VALIDO, "cualquiera"));
	}
	

	

}
