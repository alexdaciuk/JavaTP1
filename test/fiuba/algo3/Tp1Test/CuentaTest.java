package fiuba.algo3.Tp1Test;



import org.junit.Assert;
import org.junit.Test;

import fiuba.algo.CarpetaNoExistenteException;
import fiuba.algo.ContraseniaInvalidaException;
import fiuba.algo.Cuenta;
import fiuba.algo.MailInvalidoException;

public class CuentaTest {

	private static final String MAIL_VALIDO = "tp1Java@gmail.com";
	private static final String CONTRASENIA_VALIDA = "contrasenia";

	@Test
	public void puedoCrearCuenta() throws MailInvalidoException, ContraseniaInvalidaException {

		Cuenta cuenta = new Cuenta(MAIL_VALIDO, CONTRASENIA_VALIDA);

	}

	@Test(expected = fiuba.algo.MailInvalidoException.class)
	public void tiraExcepcionMailInvalidoExceptionSiMailVacio()
			throws MailInvalidoException, ContraseniaInvalidaException {
		Cuenta cuenta = new Cuenta("", CONTRASENIA_VALIDA);
	}

	@Test(expected = fiuba.algo.ContraseniaInvalidaException.class)
	public void tiraExcepcionContraseniaInvalidaExceptionSiContraseñaVacia()
			throws MailInvalidoException, ContraseniaInvalidaException {
		Cuenta cuenta = new Cuenta(MAIL_VALIDO, "");
	}

	@Test(expected = fiuba.algo.MailInvalidoException.class)
	public void tiraExcepcionMailInvalidoExceptionSiMailNoTieneArroba()
			throws MailInvalidoException, ContraseniaInvalidaException {
		Cuenta cuenta = new Cuenta("mailSinArroba.com", CONTRASENIA_VALIDA);
	}

	@Test(expected = fiuba.algo.MailInvalidoException.class)
	public void tiraExcepcionMailInvalidoExceptionSiMailNoTienePuntoCom()
			throws MailInvalidoException, ContraseniaInvalidaException {
		Cuenta cuenta = new Cuenta("mailSin@PuntoCom", CONTRASENIA_VALIDA);
	}

	@Test
	public void cuentaSeCreaConBandejaPrincipalVacia() throws MailInvalidoException, ContraseniaInvalidaException, CarpetaNoExistenteException {
		Cuenta cuenta = new Cuenta(MAIL_VALIDO, CONTRASENIA_VALIDA);
		
		cuenta.agregarCarpeta("Principal");
		
		Assert.assertEquals(0, cuenta.mensajesEnCarpeta("Principal"));
	}

}
