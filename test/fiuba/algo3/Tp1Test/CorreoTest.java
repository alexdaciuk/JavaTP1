package fiuba.algo3.Tp1Test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fiuba.algo.ContraseniaInvalidaException;
import fiuba.algo.Correo;
import fiuba.algo.Cuenta;
import fiuba.algo.MailInvalidoException;

public class CorreoTest {

	private Correo correo;

	@Before
	public void setup() throws MailInvalidoException, ContraseniaInvalidaException {
		List<String> lista = Arrays.asList("algotp1@gmail.com", "algo3tp1@gmail.com");
		Cuenta cuentaJoel = new Cuenta("joel@gmail.com", "contrasenia");
		Cuenta cuentaMarcelo = new Cuenta("marcelo@gmail.com", "contrasenia");
		correo = new Correo(cuentaJoel, lista, cuentaMarcelo, "Prueba", "Probando constructor de Mensaje");
	}

	@Test
	public void puedoCrearCorreoConRemitenteListaDestinatarioAsuntoYMensaje()
			throws MailInvalidoException, ContraseniaInvalidaException {
		List<String> lista = Arrays.asList("algotp1@gmail.com", "algo3tp1@gmail.com");
		Cuenta cuentaJose = new Cuenta("jose@gmail.com", "contrasenia");
		Cuenta cuentaJuan = new Cuenta("juan@gmail.com", "contrasenia");
		Correo correo = new Correo(cuentaJose, lista, cuentaJuan, "Prueba", "Probando constructor de Mensaje");
	}

	@Test
	public void puedoObtenerRemitenteCorreo() {
		Assert.assertEquals("joel@gmail.com", this.correo.obtenerRemitente().obtenerNombre());

	}

	@Test
	public void puedoObtenerDestinatarioCorreo() {
		Assert.assertEquals("marcelo@gmail.com", this.correo.obtenerDestinatario().obtenerNombre());

	}

	@Test
	public void puedoObtenerAsuntoCorreo() {
		Assert.assertEquals("Prueba", this.correo.obtenerAsunto());

	}

	@Test
	public void puedoObtenerCopiadosCorreo() {
		Assert.assertTrue(this.correo.obtenerCopiados().contains("algotp1@gmail.com"));
	}
	
	
}
