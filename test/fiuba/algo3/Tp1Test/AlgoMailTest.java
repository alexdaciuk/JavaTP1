package fiuba.algo3.Tp1Test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import fiuba.algo.AlgoMail;

public class AlgoMailTest {

	private static final String MAIL_VALIDO = "tp1Java@gmail.com";
	private static final String CONTRASENIA_VALIDA = "contrasenia";
	private AlgoMail clienteCorreo;

	@Before
	public void setup() {
		clienteCorreo = new AlgoMail();
		clienteCorreo.nuevaCuenta(MAIL_VALIDO, CONTRASENIA_VALIDA);
	}

	@Test
	public void puedoCrearUnAlgoMail() {
		AlgoMail clienteCorreo = new AlgoMail();
	}

	@Test
	public void puedoCrearCuentaConUsuario() {
		Assert.assertTrue(clienteCorreo.nuevaCuenta("hola@gmail.com", CONTRASENIA_VALIDA));
	}

	@Test
	public void puedoCrearCuentaConContrasenia() {
		Assert.assertTrue(clienteCorreo.nuevaCuenta("cualquier@mail.com", "cualquierCosa"));
	}

	@Test
	public void fallaSiMailVacio() {
		Assert.assertFalse(clienteCorreo.nuevaCuenta("", CONTRASENIA_VALIDA));

	}

	@Test
	public void fallaSiMailNoTieneArroba() {
		Assert.assertFalse(clienteCorreo.nuevaCuenta("mailSinArroba.com", CONTRASENIA_VALIDA));
	}

	@Test
	public void fallaSiMailNoTienePuntoCom() {
		Assert.assertFalse(clienteCorreo.nuevaCuenta("mailSin@puntoCom", CONTRASENIA_VALIDA));
	}

	@Test
	public void puedoLoguearUsuarioValido() {
		Assert.assertTrue(clienteCorreo.loginUsuario(MAIL_VALIDO, CONTRASENIA_VALIDA));
	}

	@Test
	public void fallaSiDatosLogueoTieneContraseniaInvalida() {
		Assert.assertFalse(clienteCorreo.loginUsuario(MAIL_VALIDO, "cualquiera"));
	}

	@Test
	public void fallaSiDatosDeLogueoTieneContraseniaVacia() {
		Assert.assertFalse(clienteCorreo.loginUsuario(MAIL_VALIDO, ""));
	}

	@Test
	public void fallaSiDatosDeLogueoTieneUsuarioInexistente() {
		Assert.assertFalse(clienteCorreo.loginUsuario("mengano@gmail.com", "mengano"));
	}

}
